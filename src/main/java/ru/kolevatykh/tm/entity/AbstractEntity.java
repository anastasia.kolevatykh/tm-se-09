package ru.kolevatykh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
@NoArgsConstructor(force = true)
public abstract class AbstractEntity {

    @NotNull
    protected final String id = UUID.randomUUID().toString();
}
