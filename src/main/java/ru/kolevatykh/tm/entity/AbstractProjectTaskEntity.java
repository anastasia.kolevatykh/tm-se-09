package ru.kolevatykh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.enumerate.StatusType;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractProjectTaskEntity extends AbstractEntity {

    @Nullable
    protected String userId;

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "";

    @Nullable
    protected Date startDate;

    @Nullable
    protected Date endDate;

    @NotNull
    protected StatusType statusType = StatusType.PLANNED;


    AbstractProjectTaskEntity(@NotNull final String name, @NotNull final String description,
                              @Nullable final Date startDate, @Nullable final Date endDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
