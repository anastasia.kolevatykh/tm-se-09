package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.command.AbstractCommand;

import java.util.Collection;

public interface ServiceLocator {
    @NotNull IUserService getUserService();

    @NotNull IProjectService getProjectService();

    @NotNull ITaskService getTaskService();

    @NotNull Collection<AbstractCommand> getCommands();
}
