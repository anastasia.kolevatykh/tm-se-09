package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

import java.util.ArrayList;
import java.util.List;

public final class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tr";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tRemove selected task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[TASK REMOVE]\nEnter task id: ");
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();

        if (id.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final Task task = serviceLocator.getTaskService().findOneById(userId, id);

        if (task == null) {
            throw new Exception("[The task '" + id + "' does not exist!]");
        }

        serviceLocator.getTaskService().remove(userId, id);
        System.out.println("[OK]");
    }
}
