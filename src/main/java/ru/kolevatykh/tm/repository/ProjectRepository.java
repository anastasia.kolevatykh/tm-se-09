package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.api.IProjectRepository;
import ru.kolevatykh.tm.entity.Project;

public final class ProjectRepository extends AbstractProjectTaskRepository<Project> implements IProjectRepository {

    @Override
    public void persist(@NotNull final Project project) {
        map.put(project.getId(), project);
    }
}
