package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.enumerate.StatusType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

import java.util.ArrayList;
import java.util.List;

public final class ProjectUpdateStatusCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "project-update-status";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pus";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected project status.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[PROJECT UPDATE STATUS]\nEnter project id: ");
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();

        if (id.isEmpty()) {
            throw new Exception("[The project id can't be empty.]");
        }

        @Nullable final Project project = serviceLocator.getProjectService().findOneById(userId, id);

        if (project == null) {
            throw new Exception("[The project id '" + id + "' does not exist!]");
        }

        System.out.println("Enter new STATUS name: PLANNED INPROCESS READY");
        @NotNull final String statusNew = ConsoleInputUtil.getConsoleInput();

        if (statusNew.isEmpty()) {
            throw new Exception("[The status can't be empty.]");
        }

        switch(StatusType.valueOf(statusNew)) {
            case PLANNED:
                project.setStatusType(StatusType.PLANNED);
                break;
            case INPROCESS:
                project.setStatusType(StatusType.INPROCESS);
                break;
            case READY:
                project.setStatusType(StatusType.READY);
                break;
        }

        serviceLocator.getProjectService().merge(project);
        System.out.println("[OK]");
    }
}
