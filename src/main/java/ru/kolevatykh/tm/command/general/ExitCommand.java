package ru.kolevatykh.tm.command.general;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class ExitCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "e";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\t\tExit.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[Goodbye!]");
        System.exit(0);
    }
}
