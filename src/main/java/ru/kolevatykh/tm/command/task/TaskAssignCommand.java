package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

import java.util.ArrayList;
import java.util.List;

public final class TaskAssignCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "task-assign";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ta";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tAssign task to project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[ASSIGN TASK TO PROJECT]\nEnter task id for assignment:");
        @NotNull final String taskId = ConsoleInputUtil.getConsoleInput();

        if (taskId.isEmpty()) {
            throw new Exception("[The task id can't be empty.]");
        }

        @Nullable final Task task = serviceLocator.getTaskService().findOneById(userId, taskId);

        if (task == null) {
            throw new Exception("[The task id '" + taskId + "' does not exist!]");
        }

        System.out.println("Enter project id to assign task to:");
        @NotNull final String projectId = ConsoleInputUtil.getConsoleInput();

        if (projectId.isEmpty()) {
            throw new Exception("[The project id can't be empty.]");
        }

        @Nullable final Project project = serviceLocator.getProjectService().findOneById(userId, projectId);

        if (project == null) {
            throw new Exception("[The project id '" + projectId + "' does not exist!]");
        }

        serviceLocator.getTaskService().assignToProject(userId, taskId, projectId);
        System.out.println("[OK]");
    }
}
