# Task Manager

### Описание

Приложение для создания списков задач.

### Требования к software:
1. Java 1.8

### Технологический стек:
1. Java 1.8
2. Maven 3.8.0

### Имя и контакты разработчика

Anastasia Kolevatykh

anastasia.kolevatykh@gmail.com

### Команды для сборки приложения:
```
mvn clean install
```
### Команды для запуска приложения:
```
java -jar target/task-manager-1.0.0.jar
```


