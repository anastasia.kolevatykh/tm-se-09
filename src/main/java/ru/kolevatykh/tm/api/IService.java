package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {
    @NotNull List<T> findAll();

    @Nullable T findOneById(@Nullable String id);

    void persist(@Nullable T entity);

    void merge(@Nullable T entity);

    void remove(@Nullable String id);

    void removeAll();
}
