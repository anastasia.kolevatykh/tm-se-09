package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class ProjectClearCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pcl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tRemove all projects.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[PROJECT CLEAR]");
        serviceLocator.getTaskService().removeTasksWithProjectId(userId);
        serviceLocator.getProjectService().removeAll(userId);
        System.out.println("[Removed all projects with tasks.]\n[OK]");
    }
}
